#include <iostream>
#include <fstream>
#include <sstream>
#include <vector>
#include <cstdlib>
#include <array>

#include "tools.h"
#include "kd_tree.h"

#include <Eigen/Core>
#include <Eigen/StdVector>
#include <Eigen/Eigenvalues> 

using namespace Eigen;
using namespace std;




int main(){

    //Set the number of points to visualize in the plot
    int max_poit_to_visualize=5000;

    srand ( time(NULL) );
    VectorXdVector point_cloud_a, point_cloud_b, point_cloud_z;

    ofstream file_a_out, file_b_out, file_z_out;
    file_a_out.open ("./src/reduced_cloud_a.txt");
    file_b_out.open ("./src/reduced_cloud_b.txt");
    file_z_out.open ("./src/reduced_cloud_z.txt");
 

    //open dataset
    std::fstream file_a("./src/point_cloud_a.txt", std::ios::in);
    std::fstream file_b("./src/point_cloud_b.txt", std::ios::in);
    std::fstream file_z("./src/point_cloud_z.txt", std::ios::in);
    if(!file_a.is_open() || !file_b.is_open()){
           std::cout << "Dataset not found!\n";
            return 1;
            }

    point_cloud_a=readData(file_a);
    point_cloud_b=readData(file_b);
    point_cloud_z=readData(file_z);

    int number_of_points_a = point_cloud_a.size();
    int number_of_points_b = point_cloud_b.size();
    int number_of_points_z = point_cloud_z.size();

    std::vector<int> index_list_a(number_of_points_a);
    std::iota(index_list_a.begin(), index_list_a.end(), 0);

    std::vector<int> index_list_b(number_of_points_b);
    std::iota(index_list_b.begin(), index_list_b.end(), 0);

    std::vector<int> index_list_z(number_of_points_z);
    std::iota(index_list_z.begin(), index_list_z.end(), 0);
    
    for (int i=0;i<max_poit_to_visualize;i++){
        int random_index = rand() % number_of_points_a;
        file_a_out << point_cloud_a[random_index].transpose()<<"\n";

        random_index = rand() % number_of_points_b;
        file_b_out << point_cloud_b[random_index].transpose() << "\n";

        random_index = rand() % number_of_points_z;
        file_z_out << point_cloud_z[random_index].transpose() << "\n";


    }


    file_a_out.close();
    file_b_out.close();
    file_z_out.close();
}