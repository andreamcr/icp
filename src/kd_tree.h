#pragma once
#include <iostream>
#include <vector>
#include <fstream>
#include <sstream>
#include <vector>

#include <Eigen/Core>
#include <Eigen/StdVector>
#include <Eigen/Eigenvalues> 
#include "tools.h"
using namespace Eigen;

struct Node {
  int id, left_node_id, right_node_id;
  Vector3d mean, normal;
  bool left_end, right_end;
} ;

struct LeafNode {
  int id;
  std::vector<int> target_points;
} ;


void EvaluateMeanAndNorm(const VectorXdVector& points, Vector3d& mean, Vector3d& normal, std::vector<int>& index_list);
void SplitPoints(const VectorXdVector& points, Vector3d& mean, Vector3d& normal, 
                  std::vector<int>& left_branch, std::vector<int>& right_branch, std::vector<int>& index_list);
int CreateTree (const VectorXdVector& points, std::vector<int>& index_list, std::vector<Node>& node_array, std::vector<LeafNode>& leaf_node_array);
int TraverseTree(const Vector3d& point, std::vector<Node>& node_array, int node_id);
int ComputeAssociation (const Vector3d& point, const std::vector<int>& target_points, const VectorXdVector& point_cloud);