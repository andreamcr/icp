#include "icp.h"
#include "geometric.h"


//Isometry3f

void BoxPlus(Isometry3d& Rt, Matrix<double, 6, 1> delta_x){
    Matrix3d R;
    Vector3d t, delta_t, delta_alpha;
    R=Rt.linear();
    t=Rt.translation();
    delta_t=delta_x.block<3,1>(0,0);
    delta_alpha=delta_x.block<3,1>(3,0);


    Matrix3d R_a=R_alpha(delta_alpha);
    Matrix3d new_R = R_a*R;

    Vector3d new_t = R_a*t+delta_t;

    Rt.linear()=new_R;
    Rt.translation()=new_t;
}


float ComputeErrorOnLR(const VectorXdVector& X, const VectorXdVector& Z){
    double error=0;
    int dimension=X.size();
    float inverse_dim=1.0d/dimension;

    for (int i=0;i<dimension;i++){
        error+=(X[i]-Z[i]).lpNorm<2>();
    }

    error*=inverse_dim;

    return error;
}


VectorXdVector ComputePrediction(const VectorXdVector X, Isometry3d Rt){
    VectorXdVector H;
    Vector3d h;

    Matrix3d R=Rt.linear();
    Vector3d t=Rt.translation();

    for(int i=0; i<X.size();i++){
        h=R*X[i]+t;
        H.push_back(h);
    }

    return H;
}

Vector3d ComputePredictionOn1Point(const Vector3d X, Isometry3d Rt){
    Vector3d H;

    Matrix3d R=Rt.linear();
    Vector3d t=Rt.translation();

    H=R*X+t;

    return H;
}

Isometry3d LinearRelaxation(const VectorXdVector& X, const VectorXdVector& P){
    int dimension=X.size();
    float inverse_dim=1.0d/dimension;
    Vector3d X_mean, P_mean;
    MatrixXd X_delta(3, dimension), P_delta(3, dimension);
    X_mean.setZero();
    P_mean.setZero();

    for (int i=0; i< dimension; i++){
        X_mean += X[i];
        P_mean += P[i];
    }

    X_mean*=inverse_dim;
    P_mean*=inverse_dim;

    for (int i=0; i< dimension; i++){
        X_delta.col(i)=X[i]-X_mean;
        P_delta.col(i)=P[i]-P_mean;
    }

    Matrix3d A=X_delta*P_delta.transpose();

    JacobiSVD<MatrixXd> svd(A, ComputeThinU | ComputeThinV);
    MatrixXd U=svd.matrixU();
    MatrixXd V=svd.matrixV();
    
    MatrixXd R=U*V.transpose();

    Isometry3d Rt;
    Rt.linear()=R;
    MatrixXd t=X_mean-R*P_mean;
    Rt.translation()=t;

    return Rt;
}

MatrixXd computeJacobian(Vector3d p){
    Matrix<double, 3, 6> j;

    j.block<3,3>(0,0).setIdentity();

    // -skew symmetric matrix
    Matrix<double, 3, 3> temp;
    temp << 0.0d, p(2,0), -p(1,0),         
            -p(2,0), 0.0d, p(0,0),
            p(1,0), -p(0,0), 0.0d;  

    
    j.block<3,3>(0,3)=temp;
    
    return j;
}