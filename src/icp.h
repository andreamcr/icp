#pragma once
#include <iostream>
#include <vector>
#include <fstream>
#include <sstream>
#include <vector>
#include "tools.h"

#include <Eigen/Core>
#include <Eigen/StdVector>
#include <Eigen/Eigenvalues> 
using namespace Eigen;

struct RotoTrans{
    Matrix<float, 3, 3> rotation;
    Matrix<float, 3, 1> translation;
};



void BoxPlus(Isometry3d& Rt, Matrix<double, 6, 1> delta_x);
float ComputeErrorOnLR(const VectorXdVector& X, const VectorXdVector& Z);
VectorXdVector ComputePrediction(const VectorXdVector X, Isometry3d Rt);
Vector3d ComputePredictionOn1Point(const Vector3d X, Isometry3d Rt);
Isometry3d LinearRelaxation(const VectorXdVector& X, const VectorXdVector& P);
MatrixXd computeJacobian(Vector3d p);