#pragma once
#include <iostream>
#include <vector>
#include <fstream>
#include <sstream>
#include <vector>

#include <Eigen/Core>
#include <Eigen/StdVector>
#include <Eigen/Eigenvalues> 
using namespace Eigen;

typedef std::vector<Eigen::Vector3d, Eigen::aligned_allocator<Eigen::Vector3d> > VectorXdVector;




void printVector(const VectorXdVector& input);
VectorXdVector readData(std::istream& input);
MatrixXd ConvertToEigenMatrix(VectorXdVector data);
void CreateFileForPlot(const VectorXdVector& dataset, const std::string name);