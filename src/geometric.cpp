#include "geometric.h"

#define PI 3.14159265

Matrix3d Rx(double x){
    double c = cos(x);
    double s = sin(x);
    Matrix3d r;
    r << 1,0,0,
        0,c,-s,
        0,s,c;
    return r;
}

Matrix3d Ry(double x){
    double c = cos(x);
    double s = sin(x);
    Matrix3d r;
    r << c,0,s,
                 0,1,0,
                -s,0,c;
    return r;
}

Matrix3d Rz(double x){
    double c = cos(x);
    double s = sin(x);
    Matrix3d r;
    r << c,-s,0,
                s,c,0,
                0,0,1;
    return r;
}


Matrix3d Rx_p(double x){
    double c = cos(x);
    double s = sin(x);
    Matrix3d r;
    r << 0,0,0,
                0,-c,s,
                0,-s,-c;
    return r;
}

Matrix3d Ry_p(double x){
    double c = cos(x);
    double s = sin(x);
    Matrix3d r;
    r << -c,0,-s,
                 0,0,0,
                s,0,-c;
    return r;
}

Matrix3d Rz_p(double x){
    double c = cos(x);
    double s = sin(x);
    Matrix3d r;
    r << -c,s,0,
                -s,-c,0,
                0,0,0;
    return r;
}

MatrixXd R_alpha(MatrixXd delta_alpha){

    MatrixXd Ra=Rx(delta_alpha(0,0))*Ry(delta_alpha(1,0))*Rz(delta_alpha(2,0));

    return Ra;
    }