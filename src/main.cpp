#include <iostream>
#include <fstream>
#include <sstream>
#include <vector>
#include <cstdlib>
#include <array>
#include <math.h>  

#include "tools.h"
#include "kd_tree.h"
#include "icp.h"
#include "geometric.h"

#include <Eigen/Core>
#include <Eigen/StdVector>
#include <Eigen/Eigenvalues> 

using namespace Eigen;

int main(){
    srand ( time(NULL) );
    VectorXdVector point_cloud_a, point_cloud_b, point_cloud_z;
    VectorXdVector sample_cloud_a, sample_cloud_b, sample_cloud_z;
    std::vector<Node> node_array;
    std::vector<LeafNode> leaf_node_array;
    Vector3d current_point;
    int target_leaf, current_index, nearest_point_index;
    int number_of_samples=3;
    int number_of_sampling=10000;
    std::vector<std::vector<int> > association_list; // point a | point b
    std::vector<int> target_points;
    std::vector<int> associated_points;
    double best_error=10000;
    Isometry3d initial_guess;
    Matrix<double, 3, 6> J;
    Matrix<double, 6, 6> H;
    Matrix<double, 6, 1> b, delta_X;
    int index_z, index_p;
    Vector3d current_z, current_p, z_hat, error;

    //open dataset
    std::fstream file_a("./src/point_cloud_a.txt", std::ios::in);
    std::fstream file_b("./src/point_cloud_b.txt", std::ios::in);
    if(!file_a.is_open() || !file_b.is_open()){
           std::cout << "Dataset not found!\n";
            return 1;
            }

    std::cout<<"Collecting points... ";
    point_cloud_a=readData(file_a);
    point_cloud_b=readData(file_b);

    
    int number_of_points_a = point_cloud_a.size();
    int number_of_points_b = point_cloud_b.size();
    std::cout<<" founded "<<number_of_points_a << " points in cloud A and "<<number_of_points_b << " in cloud B." << std::endl;

    std::vector<int> index_list_a(number_of_points_a);
    std::iota(index_list_a.begin(), index_list_a.end(), 0);

    std::vector<int> index_list_b(number_of_points_b);
    std::iota(index_list_b.begin(), index_list_b.end(), 0);
    
    std::cout<< "Start creating the tree... "<<std::endl;
    const int root = CreateTree(point_cloud_a, index_list_a, node_array, leaf_node_array);

    std::cout<<node_array.size()<<" nodes and ";
    std::cout<<leaf_node_array.size()<< " leaf nodes created."<<std::endl;

    std::cout<<"Starting RANSAC for "<< number_of_samples <<" sample points and "<< number_of_sampling <<" iteration"<<std::endl;
    for (int i=0; i<number_of_sampling; i++){
        sample_cloud_a.clear();
        sample_cloud_b.clear();
        sample_cloud_z.clear();
        for (int j=0;j<number_of_samples;j++) {
            //random sample and data association
            current_index = rand() % number_of_points_b;
            Vector3d current_point=point_cloud_b[current_index];
            target_leaf=TraverseTree(current_point, node_array, root);
            target_points=leaf_node_array[target_leaf].target_points;
            nearest_point_index = ComputeAssociation(current_point, target_points, point_cloud_a);
            if (nearest_point_index != -1){
                associated_points={nearest_point_index, current_index};
                association_list.push_back(associated_points);
                sample_cloud_a.push_back(point_cloud_a[nearest_point_index]);
                sample_cloud_b.push_back(current_point);
            }
            else {
                j--; // redo the entire step
            } 
        }

        //icp with linear relaxation
        Isometry3d Rt = LinearRelaxation(sample_cloud_a, sample_cloud_b);
        sample_cloud_z =ComputePrediction(sample_cloud_b, Rt);
        double init_error=ComputeErrorOnLR(sample_cloud_b, sample_cloud_a);
        double fin_error=ComputeErrorOnLR(sample_cloud_z, sample_cloud_a);

        //std::cout<<"Initial error: "<< init_error << std::endl;
        if(fin_error<best_error){
            best_error=fin_error;
            initial_guess=Rt;
        }
    }
    std::cout<<"Best error with linear relxation: "<< best_error << std::endl;
    Isometry3d state_X=initial_guess;
    point_cloud_z =ComputePrediction(point_cloud_b, state_X);
    int n_iter=50;
    std::vector<double> error_list;
    std::cout<<"-------------------------------------------------------------------------------"<<std::endl;
    for (int iter=0;iter<n_iter; iter++){
        double chi_square =0;
        int number_of_points_z = point_cloud_z.size();
        H.setZero();
        b.setZero();
        association_list.clear();
        for(int i=0; i<number_of_points_z; i++) {
            current_point = point_cloud_z[i];
            target_leaf=TraverseTree(current_point, node_array, root);
            target_points=leaf_node_array[target_leaf].target_points;
            nearest_point_index = ComputeAssociation(current_point, target_points, point_cloud_a);
            if(nearest_point_index==-1){
                continue;
            }
            associated_points={nearest_point_index, i};
            association_list.push_back(associated_points);
        }

        for (int p=0;p<association_list.size();p++){
            index_z=association_list[p][0];
            index_p=association_list[p][1];
            current_z = point_cloud_a[index_z];
            current_p = point_cloud_z[index_p];
            z_hat=ComputePredictionOn1Point(current_p, state_X);
            error=z_hat - current_z;
            J=computeJacobian(z_hat);
            chi_square+=error.squaredNorm();
            H+=J.transpose()*J;
            b+=J.transpose()*error;
        }

        delta_X=H.ldlt().solve(-b);
        BoxPlus(state_X, delta_X);
        double updated_error = chi_square/(double)association_list.size();

        point_cloud_z =ComputePrediction(point_cloud_z, state_X);
        
        std::cout<<"Iteration : "<< iter;
        std::cout<<"\t | assciated points : "<< association_list.size();
        std::cout<<"\t | mean squared error : "<< updated_error << std::endl;

        error_list.push_back(updated_error);
    
    }
    std::cout<<"-------------------------------------------------------------------------------"<<std::endl;
    
    CreateFileForPlot(point_cloud_z, "./src/point_cloud_z.txt");

    //Plot the error
    std::ofstream error_file;
    error_file.open("./src/errors.txt");
    
    for (int i=0;i<error_list.size();i++){
        error_file << i <<"  "<<error_list[i]<< "\n";
        }

    error_file.close();

}