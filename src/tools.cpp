#include "tools.h"


void printVector(const VectorXdVector& input)
{
	for (auto i : input) {
		std::cout << i << " ";
	}
    std::cout<<"\n"<<std::endl;
}

VectorXdVector readData(std::istream& input){
    VectorXdVector output;
    std::string line;
    // read every line from the stream
    while( getline(input, line) ){
        std::istringstream input_line(line);
        Matrix<double, 1, 3> point;
        input_line >> point(0,0) >> point(0,1) >> point(0,2);
        output.push_back(point);
    }
    return output;
}

MatrixXd ConvertToEigenMatrix(VectorXdVector data)
{
    Eigen::MatrixXd eMatrix(data.size(), data[0].size());
    for (int i = 0; i < data.size(); ++i)
        eMatrix.row(i) = VectorXd::Map(&data[i][0], data[0].size());
    return eMatrix;
}

void CreateFileForPlot(const VectorXdVector& dataset, const std::string name){
    using namespace std;
    ofstream file;
    file.open(name);
    
    for (int i=0;i<dataset.size();i++){
        file << dataset[i].transpose() << "\n";
        }

    file.close();
}