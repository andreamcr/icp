#include "kd_tree.h"

int node_id_counter=0;
int leaf_id_counter=0;


void EvaluateMeanAndNorm(const VectorXdVector& points, Vector3d& mean, Vector3d& normal, std::vector<int>& index_list){
    int dimension=index_list.size();
    double inverse_dim=1.0d/dimension;
    MatrixXd covariance(3, 3);
    mean.setZero();
    covariance.setZero();
    normal.setZero();

     
    //evaluate mean
    for(auto i : index_list){
        mean+=points[i];
        }
    mean*=inverse_dim;
    //evaluate covariance
    for(auto i : index_list){
        Vector3d delta=points[i]-mean;
        covariance+=delta*delta.transpose();
        }
    covariance*=inverse_dim;
    //evaluate norm
    Eigen::SelfAdjointEigenSolver<MatrixXd> solver;
    solver.compute(covariance, Eigen::ComputeEigenvectors);
    normal=solver.eigenvectors().col(2).normalized();
}

void SplitPoints(const VectorXdVector& points, Vector3d& mean, Vector3d& normal, 
                  std::vector<int>& left_branch, std::vector<int>& right_branch, std::vector<int>& index_list){
    int dimension=index_list.size();
    for(auto i : index_list){
        float distance_from_plane=normal.dot(points[i]-mean);
        if (distance_from_plane<0){
            left_branch.push_back(i);
        } else {
            right_branch.push_back(i);
        }
    }
}

int CreateTree (const VectorXdVector& points, std::vector<int>& index_list, std::vector<Node>& node_array, std::vector<LeafNode>& leaf_node_array){
    int min_size=100;
    Vector3d mean, normal;
    std::vector<int> right_branch, left_branch;
    int left_id, right_id;
    bool left_end=false, right_end=false;
    EvaluateMeanAndNorm(points, mean, normal, index_list);
    SplitPoints(points, mean, normal, left_branch, right_branch, index_list);
    if (left_branch.size()>min_size){
        left_id= CreateTree(points, left_branch, node_array, leaf_node_array);
        } else{
            left_id=leaf_id_counter;
            LeafNode current_leaf= {leaf_id_counter, left_branch};
            leaf_node_array.push_back(current_leaf);
            left_end=true;
            leaf_id_counter++;
        }
    if (right_branch.size()>min_size){
        right_id= CreateTree(points, right_branch, node_array, leaf_node_array);
        } else{
            right_id=leaf_id_counter;
            LeafNode current_leaf= {leaf_id_counter, right_branch};
            leaf_node_array.push_back(current_leaf);
            right_end=true;
            leaf_id_counter++;
        }
        
    int current_node_id=node_id_counter;
    node_id_counter++;
    Node current_node = {current_node_id, left_id, right_id, mean, normal, left_end, right_end};
    node_array.push_back(current_node);
    return current_node_id;
}


int TraverseTree(const Vector3d& point, std::vector<Node>& node_array, int node_id){
    int leaf_id=-1, next_node;
    Node current_node=node_array[node_id];
    Vector3d mean=current_node.mean;
    Vector3d normal=current_node.normal;
    
    float distance_from_plane=normal.dot(point-mean);

    if (distance_from_plane<0){
        next_node=current_node.left_node_id;
        if(current_node.left_end==false){
            leaf_id= TraverseTree(point, node_array, next_node);
        } else {
            leaf_id=next_node;
                }
    } else {
        next_node=current_node.right_node_id;
        if(current_node.right_end==false){
            leaf_id= TraverseTree(point, node_array, next_node);
        } else {
            leaf_id=next_node;
                }
        }
    return leaf_id;
}

int ComputeAssociation (const Vector3d& point, const std::vector<int>& target_points, const VectorXdVector& point_cloud){
        double best_distance=0.5; //use it as a threshold
        int guess=-1;
        for(auto id : target_points){
            Vector3d current_point=point_cloud[id];
            Vector3d distance_vector = current_point - point;
            distance_vector = distance_vector.cwiseProduct(distance_vector);
            double distance = sqrt(distance_vector.sum());

            if (distance<best_distance){
                    best_distance=distance;
                    guess = id;                    
                }
            }
        return guess;
}