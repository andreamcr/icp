#pragma once
#include <iostream>
#include <vector>
#include <fstream>
#include <sstream>
#include <vector>
#include <math.h>  

#include <Eigen/Core>
#include <Eigen/StdVector>
#include <Eigen/Eigenvalues> 
using namespace Eigen;

Matrix3d Rx(double x);
Matrix3d Ry(double x);
Matrix3d Rz(double x);
Matrix3d Rx_p(double x);
Matrix3d Ry_p(double x);
Matrix3d Rz_p(double x);
MatrixXd R_alpha(MatrixXd delta_alpha);