# CMAKE generated file: DO NOT EDIT!
# Generated by "Unix Makefiles" Generator, CMake Version 3.5

# Delete rule output on recipe failure.
.DELETE_ON_ERROR:


#=============================================================================
# Special targets provided by cmake.

# Disable implicit rules so canonical targets will work.
.SUFFIXES:


# Remove some rules from gmake that .SUFFIXES does not remove.
SUFFIXES =

.SUFFIXES: .hpux_make_needs_suffix_list


# Suppress display of executed commands.
$(VERBOSE).SILENT:


# A target that is always out of date.
cmake_force:

.PHONY : cmake_force

#=============================================================================
# Set environment variables for the build.

# The shell in which to execute make rules.
SHELL = /bin/sh

# The CMake executable.
CMAKE_COMMAND = /usr/bin/cmake

# The command to remove a file.
RM = /usr/bin/cmake -E remove -f

# Escaping for special characters.
EQUALS = =

# The top-level source directory on which CMake was run.
CMAKE_SOURCE_DIR = /home/andrea/Università/Probabilistic/02-ICP++/project

# The top-level build directory on which CMake was run.
CMAKE_BINARY_DIR = /home/andrea/Università/Probabilistic/02-ICP++/project/build

# Include any dependencies generated for this target.
include src/CMakeFiles/kd_tree.dir/depend.make

# Include the progress variables for this target.
include src/CMakeFiles/kd_tree.dir/progress.make

# Include the compile flags for this target's objects.
include src/CMakeFiles/kd_tree.dir/flags.make

src/CMakeFiles/kd_tree.dir/kd_tree.cpp.o: src/CMakeFiles/kd_tree.dir/flags.make
src/CMakeFiles/kd_tree.dir/kd_tree.cpp.o: ../src/kd_tree.cpp
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green --progress-dir=/home/andrea/Università/Probabilistic/02-ICP++/project/build/CMakeFiles --progress-num=$(CMAKE_PROGRESS_1) "Building CXX object src/CMakeFiles/kd_tree.dir/kd_tree.cpp.o"
	cd /home/andrea/Università/Probabilistic/02-ICP++/project/build/src && /usr/bin/c++   $(CXX_DEFINES) $(CXX_INCLUDES) $(CXX_FLAGS) -o CMakeFiles/kd_tree.dir/kd_tree.cpp.o -c /home/andrea/Università/Probabilistic/02-ICP++/project/src/kd_tree.cpp

src/CMakeFiles/kd_tree.dir/kd_tree.cpp.i: cmake_force
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Preprocessing CXX source to CMakeFiles/kd_tree.dir/kd_tree.cpp.i"
	cd /home/andrea/Università/Probabilistic/02-ICP++/project/build/src && /usr/bin/c++  $(CXX_DEFINES) $(CXX_INCLUDES) $(CXX_FLAGS) -E /home/andrea/Università/Probabilistic/02-ICP++/project/src/kd_tree.cpp > CMakeFiles/kd_tree.dir/kd_tree.cpp.i

src/CMakeFiles/kd_tree.dir/kd_tree.cpp.s: cmake_force
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green "Compiling CXX source to assembly CMakeFiles/kd_tree.dir/kd_tree.cpp.s"
	cd /home/andrea/Università/Probabilistic/02-ICP++/project/build/src && /usr/bin/c++  $(CXX_DEFINES) $(CXX_INCLUDES) $(CXX_FLAGS) -S /home/andrea/Università/Probabilistic/02-ICP++/project/src/kd_tree.cpp -o CMakeFiles/kd_tree.dir/kd_tree.cpp.s

src/CMakeFiles/kd_tree.dir/kd_tree.cpp.o.requires:

.PHONY : src/CMakeFiles/kd_tree.dir/kd_tree.cpp.o.requires

src/CMakeFiles/kd_tree.dir/kd_tree.cpp.o.provides: src/CMakeFiles/kd_tree.dir/kd_tree.cpp.o.requires
	$(MAKE) -f src/CMakeFiles/kd_tree.dir/build.make src/CMakeFiles/kd_tree.dir/kd_tree.cpp.o.provides.build
.PHONY : src/CMakeFiles/kd_tree.dir/kd_tree.cpp.o.provides

src/CMakeFiles/kd_tree.dir/kd_tree.cpp.o.provides.build: src/CMakeFiles/kd_tree.dir/kd_tree.cpp.o


# Object files for target kd_tree
kd_tree_OBJECTS = \
"CMakeFiles/kd_tree.dir/kd_tree.cpp.o"

# External object files for target kd_tree
kd_tree_EXTERNAL_OBJECTS =

src/libkd_tree.a: src/CMakeFiles/kd_tree.dir/kd_tree.cpp.o
src/libkd_tree.a: src/CMakeFiles/kd_tree.dir/build.make
src/libkd_tree.a: src/CMakeFiles/kd_tree.dir/link.txt
	@$(CMAKE_COMMAND) -E cmake_echo_color --switch=$(COLOR) --green --bold --progress-dir=/home/andrea/Università/Probabilistic/02-ICP++/project/build/CMakeFiles --progress-num=$(CMAKE_PROGRESS_2) "Linking CXX static library libkd_tree.a"
	cd /home/andrea/Università/Probabilistic/02-ICP++/project/build/src && $(CMAKE_COMMAND) -P CMakeFiles/kd_tree.dir/cmake_clean_target.cmake
	cd /home/andrea/Università/Probabilistic/02-ICP++/project/build/src && $(CMAKE_COMMAND) -E cmake_link_script CMakeFiles/kd_tree.dir/link.txt --verbose=$(VERBOSE)

# Rule to build all files generated by this target.
src/CMakeFiles/kd_tree.dir/build: src/libkd_tree.a

.PHONY : src/CMakeFiles/kd_tree.dir/build

src/CMakeFiles/kd_tree.dir/requires: src/CMakeFiles/kd_tree.dir/kd_tree.cpp.o.requires

.PHONY : src/CMakeFiles/kd_tree.dir/requires

src/CMakeFiles/kd_tree.dir/clean:
	cd /home/andrea/Università/Probabilistic/02-ICP++/project/build/src && $(CMAKE_COMMAND) -P CMakeFiles/kd_tree.dir/cmake_clean.cmake
.PHONY : src/CMakeFiles/kd_tree.dir/clean

src/CMakeFiles/kd_tree.dir/depend:
	cd /home/andrea/Università/Probabilistic/02-ICP++/project/build && $(CMAKE_COMMAND) -E cmake_depends "Unix Makefiles" /home/andrea/Università/Probabilistic/02-ICP++/project /home/andrea/Università/Probabilistic/02-ICP++/project/src /home/andrea/Università/Probabilistic/02-ICP++/project/build /home/andrea/Università/Probabilistic/02-ICP++/project/build/src /home/andrea/Università/Probabilistic/02-ICP++/project/build/src/CMakeFiles/kd_tree.dir/DependInfo.cmake --color=$(COLOR)
.PHONY : src/CMakeFiles/kd_tree.dir/depend

