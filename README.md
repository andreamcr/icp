#ICP ++

----------------------------------------
##Introduction

The scope of this project is to develop a system able to collect two point clouds of the same scene (each of them contains a list of thousands of points in 3D coordinates) but with different views and to define
the transformation which aligns one cloud on the other.  
To solve this problem will be presented two variants of **Iterative closest point (ICP)** algorithm: a *linear relaxation* to determine an initial guess and then a 
*non-linear ICP* to actual minimize the difference between the two clouds.

---------------------------------------

##Procedure

###Preprocessing
First operation required is, obviously, to store the two entire point clouds into memory, in order to make them available for manipulation. Here appears the first problem, because the high number of points in
these clouds make the entire process really slow (about 30 - 40 minutes for each operation). A solution to speed up the execution was to change the allocation method used to store data: instead of collect
each cloud as a two-dimensional `Eigen::Matrixit` was preferred to use a vector of one-dimensional `Eigen::Matrix`, defined as:  
`std::vector<Eigen::Vector3d, Eigen::aligned_allocator<Eigen::Vector3d> >`

###Data association
The given points was not associated from one cloud to the other, so it was necessary to define an association algorithm based on **KD-Tree**.  
Implementation of the tree was performed in a recursive way, creating a series of *nodes*. This nodes split the dataset in two parts, according to an hyperplane passing through the mean of its points with a normal aligned
with the longest axis of the covariance matrix. Hence the points cloud is divided in several smaller clouds (*leaf nodes*) of fixed dimension.   
The tree is created on point cloud A only once, then it’s called every time a points from the second cloudhas to be associated. The query points traverse the three verifying in each node if they belong to theright or the left part of the set, until they reach a leaf node.  
At this point the reduced dimension of the cloud allows a *brute force* approach to find the closest correspondence, discarding association where the distance is greater than a threshold.

###Initial Guess
A very important condition for ICP algorithms is the initial guess. It’s widely demonstrated that poor initial guess leads to wrong convergences. In this case, also considering the dimension of our dataset, the best solution is to apply a **RANSAC** schema.  
The first steps consists in sampling a random minimal set of points from one cloud, associate them with the other cloud, compute a first alignment and evaluate the mean squared error. Then repeat this steps with new sets of points for several times. At the end the alignment giving the smallest error
is chosen as initial guess.  
For this purpose has been used a linear relaxation of ICP, which replace isometric transformation with an affine transformation. This technique provides a reasonable good solution (keeping in mind that it will be refined later) in just one iteration, with the advantage that it’s not influenced by initial guess.  
In the implementation has been chosen to sample 3 points at time (the minimum needed for localizationin 3D space) for 10000 iteration. There was the possibility to apply some heuristics in the chose of the sample (for example choosing points very far each other), but also with simple random picking the ICP performed good results.

###Non-linear ICP
Now that a good initial guess is available it’s possible to use it as current state and proceed with the ICP. 
At each time this algorithm, relying on the state, evaluates a prediction of the new cloud’s position and recompute data association. Then the least square method is applied, comparing the prediction with the actual position of the points. The resulting error is used to obtain a perturbation and to update the state.
Note that in this type of experiment the state is non-Euclidean, so for its update it's necessary to define an operation on manifold (*boxplus*). Measurements, instead, are Eucliadian.  
The algorithm is repeated for 50 iteration, returning each time the number of points associated (which grows while the clouds coming closer because grows the number of *inliers*) and the *mean squared error* on the current prediction. At the end of all the process the final prediction is saved in a file, available for plotting.

----------------------------------------------------------------------------

##Results

Here are reported the results obtained after one execution.
![alternativetext](img/results.png)

and this is the Mean Squared Error trend
![alternativetext](img/error.png)

Position of the 2 clouds before ICP
![alternativetext](img/init_clouds.png)

Position of the 2 clouds after ICP
![alternativetext](img/final_clouds.png)

Note that in this two last images the plotted points are only random subsets (about 5000 points) of the entire cloud, where data association was not 
cosidered during sampling.

--------------------------------------------

##Guide
From `build` folder launch:  
-  `./src/main` to execute the program;  
- `./src/reduce_dataset` to produce a reduced version of the 3 points clouds (the original 2 and the predicted one), useful for simpler display of the plots;  
- `gnuplot` with `./src/point_cloud_<a/b/z>.txt` or `./src/reduced_cloud_<a/b/z>.txt` to plot the points or `./src/errors.txt` for a plot of the errors.